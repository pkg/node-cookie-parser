# Installation
> `npm install --save @types/cookie-parser`

# Summary
This package contains type definitions for cookie-parser (https://github.com/expressjs/cookie-parser).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/cookie-parser.
## [index.d.ts](https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/cookie-parser/index.d.ts)
````ts
// Type definitions for cookie-parser 1.4
// Project: https://github.com/expressjs/cookie-parser
// Definitions by: Santi Albo <https://github.com/santialbo>
//                 BendingBender <https://github.com/BendingBender>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// TypeScript Version: 2.3

import * as express from 'express';

declare global {
    namespace Express {
        // Inject additional properties on express.Request
        interface Request {
            /**
             * This request's secret.
             * Optionally set by cookie-parser if secret(s) are provided.  Can be used by other middleware.
             * [Declaration merging](https://www.typescriptlang.org/docs/handbook/declaration-merging.html) can be used to add your own properties.
             */
            secret?: string;
        }
    }
}

declare function cookieParser(secret?: string | string[], options?: cookieParser.CookieParseOptions): express.RequestHandler;

declare namespace cookieParser {
    interface CookieParseOptions {
        decode?(val: string): string;
    }

    function JSONCookie(jsonCookie: string): object | undefined;

    function JSONCookies<T extends { [key: string]: string }>(jsonCookies: T): { [P in keyof T]: object | undefined };

    function signedCookie(cookie: string, secret: string | string[]): string | false;

    function signedCookies<T extends { [key: string]: string }>(cookies: T, secret: string | string[]): { [P in keyof T]?: string | false};
}

export = cookieParser;

````

### Additional Details
 * Last updated: Wed, 27 Apr 2022 10:31:34 GMT
 * Dependencies: [@types/express](https://npmjs.com/package/@types/express)
 * Global values: none

# Credits
These definitions were written by [Santi Albo](https://github.com/santialbo), and [BendingBender](https://github.com/BendingBender).
